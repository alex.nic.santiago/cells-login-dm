import { LitElement, html, } from 'lit-element';
import { BGADPGrantingTicketsPostV0 } from '@cells-components/bgadp-granting-tickets-v0'
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-login-dm></cells-login-dm>
```

##styling-doc

@customElement cells-login-dm
*/
export class CellsLoginDm extends LitElement {
  static get is() {
    return 'cells-login-dm';
  }

  // Declare properties
  static get properties() {
    return {
      host: { type: String },
      country: { type: String },
      version: { type: String },
    };
  }

  constructor() {
    super();
    this.host = 'https://cal-glomo.bbva.pe/SRVS_A02';
    this.country = 'pe';
    this.version = '0';
  }


  login(userId, password, consumerId){
    let dp = new BGADPGrantingTicketsPostV0(this.country, {
      host: this.host,
      version: this.version
    });

    dp.generateRequest({
      consumerId,
      userId,
      password
    })
    .then(success => {
        console.log('tsec',success.getResponseHeader('tsec'))
        this._parseLoginResponse(success)
      },
      error => {
        this._parseLoginError(error)
      }
    );
  }
  _parseLoginResponse({response}) {
    this._dispatchEvent("request-success", response);
  }
 
  _parseLoginError({response}) {
    this._dispatchEvent("request-error", response);
  }
  _dispatchEvent(event, obj) {
    this.dispatchEvent(new CustomEvent(event, {
      bubbles: true,
      composed: true,
      detail: obj,
    }));
  }
}
